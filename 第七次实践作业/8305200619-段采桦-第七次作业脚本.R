setwd("F:/大三下/R语言及其生物信息学应用")#设置工作路径
expr=read.csv("mRNA_exprSet.csv",header = TRUE,row.names = 1)
#读取csv文件，将第一列作为行名，将第一行设为列名
colnames(expr)=substring(colnames(expr),14,15)#提取分组信息
colData=data.frame(colnames(expr))#将样本信息存为数据框数据
colnames(colData)="condition"
colData[,1]=as.factor(colData[,1])#将分组信息数据类型变为factor(因子)
if (!require("BiocManager", quietly = TRUE))
  install.packages("BiocManager")
BiocManager::install("DESeq2")#安装DESeq2包
BiocManager::install("ggplot2")#安装ggplot2包
library(DESeq2)
library(ggplot2)
dds=DESeqDataSetFromMatrix(expr,colData,design = ~condition)#构造用于DESeq2的DESeqDataSet对象
head(dds)#查看前六条信息
dds=DESeq(dds)#运用DESeq进行差异表达分析
res=results(dds)#查看结果
head(res)#前六条
res <- res[order(res$padj),]
resdata<-merge(as.data.frame(res1),as.data.frame(counts(dds,normalize=TRUE)),
               by="row.names",sort=FALSE)
deseq_res<-data.frame(resdata)
deseq_res$sig <- "NotSig"#其余均为NotSig
deseq_res$sig[deseq_res$padj <= 0.05 & 2^(deseq_res$log2FoldChange) >= 1.2] <- "Up"#上调
deseq_res$sig[deseq_res$padj <= 0.05 & 2^(deseq_res$log2FoldChange) <= 1/1.2] <- "Down"#下调，后续用于颜色的设置
jpeg("DESeq2.png",width = 1000,height = 800)
ggplot(data = deseq_res,mapping = aes(x=log2FoldChange, y=-log10(pvalue))) + #使用ggplot画图，并指定x,y                         
  geom_point(aes(colour=factor(sig)),
             alpha=0.5)+
  labs(title="volcanoplot",                              
       x="log2 fold change",
       y="-log10 pvalue")+ #x,y轴标签及标题 
  scale_color_manual(values = c("green","grey","red"))+
  geom_hline(yintercept = -log10(0.05),linetype=4)+
  geom_vline(xintercept = c(log2(1/1.2),log2(1.2)),linetype=4)#加上分割线使结果表示更加清晰
dev.off()